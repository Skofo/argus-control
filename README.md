# Argus Vision
![Argus Vision](http://i.imgur.com/5ko86Ny.jpg)
# Argus Control
Argus Control is the main part of Argus Vision. 

It is used to merge the data of multiple Argus Kinect instances and send this data to any other application using OSC. 
